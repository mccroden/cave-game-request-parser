# Cave Game Request Parser

Cave game makes a couple requests that we are interested in. These will
be used to make the collaborative map between multiple users.

## Installation

### Prerequisites:

- [mitmproxy](https://mitmproxy.org/)
- Firefox (preferred as you can set proxy server settings in browser)
- Python

### Steps


1. [Follow the Installation guide for your OS.](https://docs.mitmproxy.org/stable/overview-installation/)
(Linux, give preference to trying with your system package manager.)
1. Configure Proxy Server in Firefox
    1. Open settings
    1. Search for Proxy
    1. Check the Manual proxy setting box
    1. In the http text area put `127.0.0.1` and port `8080`
    1. Check the box to use this for https proxy 
    1. Press Ok

1. Run `mitmproxy` from your command line
1. Navigate to [http://mitm.it](http://mitm.it/#Firefox). (If you see content then mitmproxy
   is installed correctly)
1. Add the mitmproxy certificate to Firefox
    1. Click on the Get... button to download the cert
    1. Open Settings
    1. In left navbar, choose Privacy & Security
    1. At the bottom of the page, under Certificates heading, click View Certificates
    1. In the pop-up, choose the Authorities tab
    1. Click Import... and select the downloaded certificate.
    1. Enable Trust this CA to identify websites and click OK.
    1. Open [https://mitmproxy.org](https://mitmproxy.org) to verify SSL is working through the proxy.

## Run

Run `mitmdump -s response.py`

## Dev Info

### Token `GET`

Clicking on a NFT sends a `GET` request to `https://cave-game-api.wolf.game/token/10407`

Response contains the following: 

```json
{
  "inventory": [],
  "alpha": 0,
  "heatGogglesUntil": 1649822822399,
  "stunnedUntil": 1649901160928,
  "detectorUntil": 1649822411132,
  "authAt": 1649949745430,
  "history": [
    902625,
    902624,
    902623,
    902622,
    901217,
    902622,
    902623,
    901218,
    899813
  ],
  "wool": 101,
  "energy": 1.902231481481465,
  "totalSteps": 326,
  "pack": 1026,
  "wallet": "0x622f53b2734887fce8236af1993070b056a8d6aa",
  "energyUpdated": 1649948583674,
  "flashlightUntil": 1649822397422,
  "id": 10407,
  "loc": 899813,
  "gemDetectorUntil": 0,
  "visible": [
    {
      "x": 615,
      "y": 642,
      "loc": 902625,
      "directions": 26,
      "movements": 1
    },
    {
      "x": 613,
      "y": 640,
      "loc": 899813,
      "directions": 22,
      "movements": 1
    },
    {
      "x": 613,
      "y": 641,
      "loc": 901218,
      "directions": 24,
      "movements": 1
    },
    {
      "x": 612,
      "y": 642,
      "loc": 902622,
      "directions": 28,
      "movements": 2
    },
    {
      "x": 612,
      "y": 641,
      "loc": 901217,
      "directions": 16,
      "movements": 1
    },
    {
      "x": 614,
      "y": 642,
      "loc": 902624,
      "directions": 22,
      "movements": 1
    },
    {
      "x": 613,
      "y": 642,
      "loc": 902623,
      "directions": 14,
      "movements": 2
    }
  ],
  "gridSize": 1405,
  "showShop": false
}
```

### Move `POST`

Moving the NFT sends a `POST` request to `https://cave-game-api.wolf.game/move`

The Response is identical to the json data from the Token `GET` request, shown 
above.
