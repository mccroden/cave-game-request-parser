from mitmproxy import http
from mitmproxy import ctx
from mitmutils import utils

from pathlib import Path
import re
import time
import json

PROJ_ROOT = Path(__file__).resolve().parent
CONFIG_FILE = PROJ_ROOT / "config.yaml"


def response(flow: http.HTTPFlow) -> None:
    matches = utils.readFile(CONFIG_FILE)
    url = flow.request.url

    if matches is not None:
        if not flow.request.method == "OPTIONS":
            for pattern_url, dump_folder_text in matches.items():
                dump_folder = PROJ_ROOT / "request" / dump_folder_text
                dump_folder.mkdir(parents=True, exist_ok=True)
                if re.match(pattern_url, url) is not None:
                    dump_file = dump_folder / f"{str(int(round(time.time() * 1000)))}"
                    ctx.log.info(f">>> Save {url} request details to {dump_file.name}")
                    param_dict = {"request": {}, "response": {}}
                    param_dict["request"]["method"] = flow.request.method
                    param_dict["request"]["url"] = flow.request.url
                    request_payload = flow.request.content.decode("utf-8")
                    if request_payload != "":
                        param_dict["request"]["payload"] = json.loads(request_payload)
                    for k, v in flow.response.headers.items():
                        param_dict["response"][k] = v
                    response_content = flow.response.content.decode("utf-8")
                    if response_content != "":
                        param_dict["response"]["content"] = json.loads(response_content)
                    with open(dump_file, "a") as file:
                        json.dump(param_dict, file)
